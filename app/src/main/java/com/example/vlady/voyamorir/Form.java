package com.example.vlady.voyamorir;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Form extends AppCompatActivity {

    EditText ingresoNombre, ingresoFecha;
    RadioGroup radioGroup, radioGroup2;
    Button siguiente2;
    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        ingresoNombre = (EditText)findViewById(R.id.editText3);
        ingresoFecha = (EditText)findViewById(R.id.editText4);

        radioGroup = (RadioGroup)findViewById(R.id.radioGroup);

        switch (radioGroup.getCheckedRadioButtonId()){
            case R.id.radioButton2:

                Toast.makeText(getBaseContext(), "Eres hombre", Toast.LENGTH_SHORT).show();
                break;

            case R.id.radioButton:
                Toast.makeText(getBaseContext(),"Eres mujer", Toast.LENGTH_SHORT).show();
                break;
        }

        radioGroup2 = (RadioGroup)findViewById(R.id.radioGroup2);

        textView = (TextView)findViewById(R.id.textView);

    }

    private String gestionarRadioButtons() {
        String frase = "";
        switch (radioGroup2.getCheckedRadioButtonId()){
            case R.id.radioButton10:

                if(radioGroup.getCheckedRadioButtonId()==R.id.radioButton2){
                    frase = " morirás degollado por tu mujer mientras duermas cuando pasen 23 años de matrimonio.";
                    // Toast.makeText(getBaseContext()," morirás degollado por tu mujer mientras duermas cuando pasen 23 años de matrimonio.",Toast.LENGTH_SHORT ).show();
                }else{
                    frase = " morirás en tu apartamento por exceso de alcohol.";
                    // Toast.makeText(getBaseContext()," morirás en tu apartamento por exceso de alcohol.",Toast.LENGTH_SHORT ).show();
                }
                break;

            case R.id.radioButton3:
                if(radioGroup.getCheckedRadioButtonId()==R.id.radioButton){
                    frase = " moriras sola por haber matado a tu marido.";
                    // Toast.makeText(getBaseContext()," moriras sola por haber matado a tu marido.",Toast.LENGTH_SHORT ).show();
                }else{
                    frase = " morirás de vieja";
                    // Toast.makeText(getBaseContext()," morirás de vieja",Toast.LENGTH_SHORT ).show();
                }
                break;

        }
        
        return frase;
    }

    public void siguiente2(View v){
        textView.setText("Eres " + ingresoNombre.getText().toString() + " con fecha de nacimiento " + ingresoFecha.getText().toString() + gestionarRadioButtons());
    }
}
